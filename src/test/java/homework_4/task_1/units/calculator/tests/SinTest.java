package homework_4.task_1.units.calculator.tests;

import homework_4.task_1.units.calculator.tests.dataProviders.CalculatorDataProvider;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SinTest extends BaseCalculatorTest {

    @Test(dataProviderClass = CalculatorDataProvider.class, dataProvider = "sinDataProvider")
    public void sinTest(double x, double expected) {
        double result = calculator.sin(x);
        Assert.assertEquals(result, expected, 0.000001);
    }

}
