package homework_4.task_1.units.calculator.tests;

import homework_4.task_1.units.calculator.tests.dataProviders.CalculatorDataProvider;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MultiplicationTest extends BaseCalculatorTest {

    @Test(dataProviderClass = CalculatorDataProvider.class, dataProvider = "multiplicationDataProviderDouble")
    public void multDoubleTest(double a, double b, double expected) {
        double result = calculator.mult(a, b);
        Assert.assertEquals(result, expected, 0.0000001);
    }

    @Test (dataProviderClass = CalculatorDataProvider.class, dataProvider = "multiplicationDataProviderLong")
    public void multLongTest(long a, long b, long expected) {
        long result = (long) calculator.mult(a, b);
        Assert.assertEquals(result, expected, 0.0000001);
    }

}
