package homework_4.task_1.units.calculator.tests.dataProviders;

import org.testng.annotations.DataProvider;

public class CalculatorDataProvider {

    @DataProvider
    public Object[][] additionDataProviderDouble () {
        return new Object [][] {
                {-100.0, 1.0, -99},
                {100.0, -1.0, 99},
                {0.0, 21.02, 21.02},
                {345.67, 78.9, 424.57}
        };
    }

    @DataProvider
    public Object[][] additionDataProviderLong () {
        return new Object [][] {
                {9223372036854775805L, 0L, 9223372036854775805L},
                {9876L, 1234567L, 1244443L},
                {-9876L, 1234567L, 1224691L},
                {9876L, -1234567L, -1224691L}
        };
    }


    @DataProvider
    public Object[][] subtractionDataProviderDouble () {
        return new Object [][] {
                {108.12, 8.03, 100.09},
                {506, 500, 6}
        };
    }

    @DataProvider
    public Object[][] subtractionDataProviderLong () {
        return new Object [][] {
                {7341474836491L, 871474838889L, 6469999997602L},
                {4555147483649L, 1714748388L, 4553432735261L},
        };
    }

    @DataProvider
    public Object[][] multiplicationDataProviderDouble () {
        return new Object [][] {
                {21.02, 19.95, 419.349},
                {-21.02, 19.95, -419.349},
                {-21.02, -19.95, 419.349},
                {0.0, 17.7, 0.0},
                {9223372036854775805L, 0L, 0L}
        };
    }

    @DataProvider
    public Object[][] multiplicationDataProviderLong () {
        return new Object [][] {
                {1457899000875L, 1L, 1457899000875L},
                {1457899000875L, 3L, 4373697002625L},
                {1457899000875L, -3L, -4373697002625L},
                {-1457899000875L, -3L, 4373697002625L},

        };
    }

    @DataProvider
    public Object[][] divisionDataProviderDouble () {
        return new Object [][] {
                {121.0, 11.0, 11.0},
                {-121.0, -11.0, 11.0},
                {121.1, 1.0, 121.1},
                {1.2, 24.0, 0.05},
                {1.2, -24.0, -0.05},
                {478.0, 0.0, Double.POSITIVE_INFINITY}
        };
    }

    @DataProvider
    public Object[][] divisionDataProviderLong () {
        return new Object [][] {
                {2347896L, 586974L, 4L},
                {2347896L, -586974L, -4L},
                {-2347896L, -586974L, 4L}
        };
    }

    @DataProvider
    public Object[][] divideNegativeDataProvider () {
        return new Object [][] {
                {734147483676490L, 0L}
        };
    }

    @DataProvider
    public Object[][] exponentiationDataProvider() {
        return new Object[][]{
                {0.0, 7112.5, 0.0},
                {7112.5, 0.0, 1.0},
                {7112.5, 1.0, 7112.5},
                {4.3, 3.0, 79.507},
                {-4.3, 3.0, -79.507},
                {4.3, -2.0, 0.0540832},
                {4.3, -0.5, 0.4822428}
        };
    }

    @DataProvider
    public Object[][] expNegativeDataProvider() {
        return new Object[][]{
                {-4.3, -0.5}
        };
    }

    @DataProvider
    public Object[][] sqrtDataProvider() {
        return new Object[][]{
                {13456.0, 116.0},
                {13457.0, 116.0043102},
                {1, 1},
                {0, 0}
        };
    }

    @DataProvider
    public Object[][] sqrtNegativeDataProvider() {
        return new Object[][]{
                {-13456.0},
        };
    }

    @DataProvider
    public Object[][] isPositiveDataProvider() {
        return new Object[][]{
                {0, false},
                {-12, false},
                {12, true}
        };
    }

    @DataProvider
    public Object[][] isNegativeDataProvider() {
        return new Object[][]{
                {0, false},
                {-12, true},
                {12, false}
        };
    }

    @DataProvider
    public Object[][] sinDataProvider() {
        return new Object[][]{
                {1.5708, 1.0},
                {0.523599, 0.5}
        };
    }


    @DataProvider
    public Object[][] cosDataProvider() {
        return new Object[][]{
                {1.5708, 0.0},
                {1.0472, 0.5}
        };
    }

    @DataProvider
    public Object[][] tgDataProvider() {
        return new Object[][]{
                {0.785398, 1.0},
                {3.14159, 0.0},
                {1.0472, 1.73205080},
                {0.523599, 0.5773502}
        };
    }

    @DataProvider
    public Object[][] сtgDataProvider() {
        return new Object[][]{
                {0.785398, 1.0},
                {1.0472, 0.57735}
        };
    }
}
