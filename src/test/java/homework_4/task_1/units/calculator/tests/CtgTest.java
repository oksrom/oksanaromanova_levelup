package homework_4.task_1.units.calculator.tests;

import homework_4.task_1.units.calculator.tests.dataProviders.CalculatorDataProvider;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CtgTest extends BaseCalculatorTest {

    @Test(dataProviderClass = CalculatorDataProvider.class, dataProvider = "сtgDataProvider")
    public void ctgTest(double x, double expected) {
        double result = calculator.ctg(x);
        Assert.assertEquals(result, expected, 0.00001);
    }

}
