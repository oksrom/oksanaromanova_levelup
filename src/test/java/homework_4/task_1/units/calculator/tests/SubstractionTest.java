package homework_4.task_1.units.calculator.tests;

import homework_4.task_1.units.calculator.tests.dataProviders.CalculatorDataProvider;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SubstractionTest extends BaseCalculatorTest{

    @Test(dataProviderClass = CalculatorDataProvider.class, dataProvider = "subtractionDataProviderDouble")
    public void subDoubleTest(double a, double b, double expected) {
        double result = calculator.sub(a, b);
        Assert.assertEquals(result, expected, 0.0000001);
    }

    @Test(dataProviderClass = CalculatorDataProvider.class, dataProvider = "subtractionDataProviderLong")
    public void subLongTest(long a, long b, long expected) {
        long result = (long) calculator.sub(a, b);
        Assert.assertEquals(result, expected, 0.0000001);
    }
}