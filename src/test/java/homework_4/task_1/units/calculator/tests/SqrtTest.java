package homework_4.task_1.units.calculator.tests;

import homework_4.task_1.units.calculator.tests.dataProviders.CalculatorDataProvider;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SqrtTest extends BaseCalculatorTest {

    @Test(dataProviderClass = CalculatorDataProvider.class, dataProvider = "sqrtDataProvider")
    public void sqrtTest (double a, double expected) {
        double result = calculator.sqrt(a);
        Assert.assertEquals(result, expected, 0.00001);
    }

    @Test(dataProviderClass = CalculatorDataProvider.class, dataProvider = "sqrtNegativeDataProvider",
            expectedExceptions = ArithmeticException.class)
    public void sqrtNegativeTest(double a) {
        double result = calculator.sqrt(a);
    }
}
