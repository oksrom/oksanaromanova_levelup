package homework_4.task_1.units.calculator.tests;

import homework_4.task_1.units.calculator.tests.dataProviders.CalculatorDataProvider;
import org.testng.Assert;
import org.testng.annotations.Test;

public class IsPositiveTest extends BaseCalculatorTest {

    @Test(dataProviderClass = CalculatorDataProvider.class, dataProvider = "isPositiveDataProvider")
    public void isPositive (long a, boolean expected) {
        boolean result = calculator.isPositive(a);
        Assert.assertEquals(result, expected);
    }

}
