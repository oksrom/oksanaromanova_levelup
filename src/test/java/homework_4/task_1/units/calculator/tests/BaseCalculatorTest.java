package homework_4.task_1.units.calculator.tests;

import com.epam.tat.module4.Calculator;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public abstract class BaseCalculatorTest {
    protected Calculator calculator;

    @BeforeMethod
    public void setUpCalculator() {
         calculator = new Calculator();
    }

    @AfterMethod
    public void tearDownCalculator() {
        calculator = null;
    }
}
