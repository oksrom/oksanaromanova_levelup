package homework_4.task_1.units.calculator.tests;

import homework_4.task_1.units.calculator.tests.dataProviders.CalculatorDataProvider;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CosTest extends BaseCalculatorTest {

    @Test(dataProviderClass = CalculatorDataProvider.class, dataProvider = "cosDataProvider")
    public void cosTest(double x, double expected) {
        double result = calculator.cos(x);
        Assert.assertEquals(result, expected, 0.000001);
    }
}
