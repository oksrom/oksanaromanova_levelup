package homework_4.task_1.units.calculator.tests;

import homework_4.task_1.units.calculator.tests.dataProviders.CalculatorDataProvider;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TgTest extends BaseCalculatorTest {

    @Test(dataProviderClass = CalculatorDataProvider.class, dataProvider = "additionDataProviderDouble")
    public void sumDoubleTest(double a, double b, double expected) {
        double result = calculator.sum (a, b);
        Assert.assertEquals(result, expected, 0.0000001);
    }

    @Test (dataProviderClass = CalculatorDataProvider.class, dataProvider = "additionDataProviderLong")
    public void sumLongTest(long a, long b, long expected) {
        long result = (long) calculator.sum (a, b);
        Assert.assertEquals(result, expected, 0.0000001);
    }

}
