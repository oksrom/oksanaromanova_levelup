package homework_4.task_1.units.calculator.tests;

import homework_4.task_1.units.calculator.tests.dataProviders.CalculatorDataProvider;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DivisionTest extends BaseCalculatorTest {

    @Test(dataProviderClass = CalculatorDataProvider.class, dataProvider = "divisionDataProviderDouble")
    public void divDoubleTest(double a, double b, double expected) {
        double result = calculator.div(a, b);
        Assert.assertEquals(result, expected, 0.0000001);
    }


    @Test(dataProviderClass = CalculatorDataProvider.class, dataProvider = "divisionDataProviderLong")
    public void divLongTest(long a, long b, long expected) {
        long result = (long) calculator.div(a, b);
        Assert.assertEquals(result, expected, 0.0000001);
    }

    @Test(dataProviderClass = CalculatorDataProvider.class, dataProvider = "divideNegativeDataProvider",
            expectedExceptions = NumberFormatException.class)
    public void divideNegativeTest(long a, long b) {
        long result = calculator.div(a, b);
    }

}
