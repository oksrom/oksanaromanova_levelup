package homework_4.task_1.units.calculator.tests;

import homework_4.task_1.units.calculator.tests.dataProviders.CalculatorDataProvider;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ExponentiationTest extends BaseCalculatorTest {

    @Test(dataProviderClass = CalculatorDataProvider.class, dataProvider = "exponentiationDataProvider")
    public void exp(double a, double b, double expected) {
        double result = calculator.pow(a,b);
        Assert.assertEquals(result, expected, 0.00001);
    }

    @Test(dataProviderClass = CalculatorDataProvider.class, dataProvider = "expNegativeDataProvider",
            expectedExceptions = ArithmeticException.class)
    public void expNegativeTest(double a, double b) {
        double result = calculator.pow(a,b);
    }

}
