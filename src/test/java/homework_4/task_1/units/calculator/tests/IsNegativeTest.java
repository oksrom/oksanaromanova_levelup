package homework_4.task_1.units.calculator.tests;

import homework_4.task_1.units.calculator.tests.dataProviders.CalculatorDataProvider;
import org.testng.Assert;
import org.testng.annotations.Test;

public class IsNegativeTest extends BaseCalculatorTest {

    @Test(dataProviderClass = CalculatorDataProvider.class, dataProvider = "isNegativeDataProvider")
    public void isNegative (long a, boolean expected) {
        boolean result = calculator.isNegative(a);
        Assert.assertEquals(result, expected);
    }

}
