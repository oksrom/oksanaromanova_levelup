package homework_8.api.rqrs;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.annotations.BeforeMethod;

public abstract class AbstractBaseTest {
    public RequestSpecification rqSpec;
    ResponseSpecification rsSpec;

    @BeforeMethod
    public void setUp() {
        rqSpec = new RequestSpecBuilder()
                .setBaseUri(Endpoints.BASE_REQ_RES_ENDPOINT)
                .addHeader("Authorization", "Bearer lqb1FCPYRRrj7ZNk4CNL1hyRjSwz2GafvOhF")
                .log(LogDetail.ALL)
                .build();

        rsSpec = new ResponseSpecBuilder()
                .expectStatusCode(201)
                .expectContentType(ContentType.JSON)
                .log(LogDetail.ALL)
                .build();
    }

}
