package homework_8.api.rqrs.posts;

import homework_8.api.rqrs.AbstractBaseTest;
import homework_8.api.rqrs.Endpoints;
import homework_8.api.rqrs.Meta;
import io.restassured.http.ContentType;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.samePropertyValuesAs;

public class PostsGoRestTestAbstract extends AbstractBaseTest {

    Meta metaPost = new Meta(true, 201, "A resource was successfully created in response to a POST request. The Location header contains the URL pointing to the newly created resource.");
    Meta metaPatch = new Meta(true, 200, "OK. Everything worked as expected.");
    Meta metaDelete = new Meta(true, 204, "The request was handled successfully and the response contains no body content.");
    Result resultPost1 = new Result(29937, "newPostsTest1", "Hi there! I am new test#1");
    Result resultPost2 = new Result(29938, "newPostsTest2", "Hi there! I am new test#2");
    Result resultPost3 = new Result(29939, "newPostsTest3", "Hi there! I am new test#3");
    Result resultPatch1 = new Result(29937, "newPostsTestAfterPatch1", "Hi there! I am updated test#1");
    Result resultPatch2 = new Result(29938, "newPostsTestAfterPatch2", "Hi there! I am updated test#2");
    Result resultPatch3 = new Result(29939, "newPostsTestAfterPatch3", "Hi there! I am updated test#3");
    PostsRequest postsPost1 = new PostsRequest(29937, "newPostsTest1", "Hi there! I am new test#1");
    PostsRequest postsPost2 = new PostsRequest(29938, "newPostsTest2", "Hi there! I am new test#2");
    PostsRequest postsPost3 = new PostsRequest(29939, "newPostsTest3", "Hi there! I am new test#3");
    PostsRequest postsPatch1 = new PostsRequest(29937, "newPostsTestAfterPatch1", "Hi there! I am updated test#1");
    PostsRequest postsPatch2 = new PostsRequest(29938, "newPostsTestAfterPatch2", "Hi there! I am updated test#2");
    PostsRequest postsPatch3 = new PostsRequest(29939, "newPostsTestAfterPatch3", "Hi there! I am updated test#3");
    PostsResponse postsPostResponse1 = new PostsResponse(metaPost, resultPost1);
    PostsResponse postsPostResponse2 = new PostsResponse(metaPost, resultPost2);
    PostsResponse postsPostResponse3 = new PostsResponse(metaPost, resultPost3);
    PostsResponse postsPatchResponse1 = new PostsResponse(metaPatch, resultPatch1);
    PostsResponse postsPatchResponse2 = new PostsResponse(metaPatch, resultPatch2);
    PostsResponse postsPatchResponse3 = new PostsResponse(metaPatch, resultPatch3);

    @DataProvider
    public Object[][] entityDataProvider() {
        return new Object[][]{
                {postsPost1, postsPostResponse1, postsPatch1, postsPatchResponse1},
                {postsPost2, postsPostResponse2, postsPatch2, postsPatchResponse2},
                {postsPost3, postsPostResponse3, postsPatch3, postsPatchResponse3}
        };
    }

    @Test(dataProvider = "entityDataProvider")
    public void postsRequestEntityDrivenTestingTest(PostsRequest postRequest, PostsResponse postResponse, PostsRequest patchRequest, PostsResponse patchResponse) {
        PostsResponse postResponseBody = given()
                .spec(rqSpec)
                .contentType(ContentType.JSON)
                .body(postRequest)
                .when()
                .post(Endpoints.POSTS_ENDPOINT)
                .as(PostsResponse.class);
        System.out.println(postResponseBody);
        assertThat(postResponseBody, samePropertyValuesAs(postResponse));
        PostsResponse patchResponseBody = given()
                .spec(rqSpec)
                .pathParam("id", postResponseBody.getResult().getId())
                .contentType(ContentType.JSON)
                .body(patchRequest)
                .when()
                .patch(Endpoints.PATCH_DELETE_POSTS_ENDPOINT)
                .as(PostsResponse.class);
        System.out.println(patchResponseBody);
        assertThat(patchResponseBody, samePropertyValuesAs(patchResponse));
        PostsResponse deleteResponseBody = given()
                .spec(rqSpec)
                .pathParam("id", postResponseBody.getResult().getId())
                .contentType(ContentType.JSON)
                .body("")
                .when()
                .delete(Endpoints.PATCH_DELETE_POSTS_ENDPOINT)
                .as(PostsResponse.class);
        System.out.println(deleteResponseBody);
        assertThat(deleteResponseBody.getMeta().getCode(), equalTo(metaDelete.getCode()));
        assertThat(deleteResponseBody.getMeta().getMessage(), equalTo(metaDelete.getMessage()));
    }
}
