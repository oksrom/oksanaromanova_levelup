package homework_8.api.rqrs;

public final class Endpoints {

    public static final String BASE_REQ_RES_ENDPOINT = "https://gorest.co.in/";

    public static final String USERS_ENDPOINT = "public-api/users";

    public static final String PATCH_DELETE_USERS_ENDPOINT = "public-api/users/{id}";

    public static final String POSTS_ENDPOINT = "public-api/posts";

    public static final String PATCH_DELETE_POSTS_ENDPOINT = "public-api/posts/{id}";

    public static final String COMMENTS_ENDPOINT = "public-api/posts";

    public static final String PATCH_DELETE_COMMENTS_ENDPOINT = "public-api/posts/{id}";

    private Endpoints() {
    }
}
