package homework_8.api.rqrs.comments;

import homework_8.api.rqrs.AbstractBaseTest;
import homework_8.api.rqrs.Endpoints;
import homework_8.api.rqrs.Meta;
import io.restassured.http.ContentType;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.samePropertyValuesAs;

public class CommentsGoRestTestAbstract extends AbstractBaseTest {

        Meta metaPost = new Meta(true, 201, "A resource was successfully created in response to a POST request. The Location header contains the URL pointing to the newly created resource.");
        Meta metaPatch = new Meta(true, 200, "OK. Everything worked as expected.");
        Meta metaDelete = new Meta(true, 204, "The request was handled successfully and the response contains no body content.");
        Result resultPost1 = new Result(61, "Myron Trantow", "mante.jazmin@gmail.com", "Placeat ipsa qui sit assumenda pariatur omnis. Repellendus ut provident expedita ipsam aspernatur. Iure consectetur laudantium voluptas non eveniet.");
        Result resultPost2 = new Result(66, "Linwood Batz", "melody.padberg@borer.com", "A earum illum corporis a iste. Maiores aperiam dolor quo sit iusto.");
        Result resultPatch1 = new Result(61, "Myron Trantow", "mante.jazmin.update@gmail.com", "Update Placeat ipsa qui sit assumenda pariatur omnis. Repellendus ut provident expedita ipsam aspernatur. Iure consectetur laudantium voluptas non eveniet.");
        Result resultPatch2 = new Result(66, "Linwood Batz", "melody.padberg.update@borer.com", "Update A earum illum corporis a iste. Maiores aperiam dolor quo sit iusto.");
        CommentsRequest commentsPost1 = new CommentsRequest(61, "Myron Trantow", "mante.jazmin@gmail.com", "Update Placeat ipsa qui sit assumenda pariatur omnis. Repellendus ut provident expedita ipsam aspernatur. Iure consectetur laudantium voluptas non eveniet.");
        CommentsRequest commentsPost2 = new CommentsRequest(66, "Linwood Batz", "melody.padberg@borer.com", "Update A earum illum corporis a iste. Maiores aperiam dolor quo sit iusto.");
        CommentsRequest commentsPatch1 = new CommentsRequest(61, "Myron Trantow", "mante.jazmin.update@gmail.com", "Update Placeat ipsa qui sit assumenda pariatur omnis. Repellendus ut provident expedita ipsam aspernatur. Iure consectetur laudantium voluptas non eveniet.");
        CommentsRequest commentsPatch2 = new CommentsRequest(66, "Linwood Batz", "melody.padberg.update@borer.com", "Update A earum illum corporis a iste. Maiores aperiam dolor quo sit iusto.");
        CommentsResponse commentsPostResponse1 = new CommentsResponse(metaPost, resultPost1);
        CommentsResponse commentsPostResponse2 = new CommentsResponse(metaPost, resultPost2);
        CommentsResponse commentsPatchResponse1 = new CommentsResponse(metaPatch, resultPatch1);
        CommentsResponse commentsPatchResponse2 = new CommentsResponse(metaPatch, resultPatch2);

        @DataProvider
        public Object[][] entityDataProvider() {
            return new Object[][]{
                    {commentsPost1, commentsPostResponse1, commentsPatch1, commentsPatchResponse1},
                    {commentsPost2, commentsPostResponse2, commentsPatch2, commentsPatchResponse2}
            };
        }

        @Test(dataProvider = "entityDataProvider")
        public void postsRequestEntityDrivenTestingTest(CommentsRequest postRequest, CommentsResponse postResponse, CommentsRequest patchRequest, CommentsResponse patchResponse) {
            CommentsResponse postResponseBody = given()
                    .spec(rqSpec)
                    .contentType(ContentType.JSON)
                    .body(postRequest)
                    .when()
                    .post(Endpoints.COMMENTS_ENDPOINT)
                    .as(CommentsResponse.class);
            System.out.println(postResponseBody);
            assertThat(postResponseBody, samePropertyValuesAs(postResponse));
            CommentsResponse patchResponseBody = given()
                    .spec(rqSpec)
                    .pathParam("id", postResponseBody.getResult().getId())
                    .contentType(ContentType.JSON)
                    .body(patchRequest)
                    .when()
                    .patch(Endpoints.PATCH_DELETE_COMMENTS_ENDPOINT)
                    .as(CommentsResponse.class);
            System.out.println(patchResponseBody);
            assertThat(patchResponseBody, samePropertyValuesAs(patchResponse));
            CommentsResponse deleteResponseBody = given()
                    .spec(rqSpec)
                    .pathParam("id", postResponseBody.getResult().getId())
                    .contentType(ContentType.JSON)
                    .body("")
                    .when()
                    .delete(Endpoints.PATCH_DELETE_COMMENTS_ENDPOINT)
                    .as(CommentsResponse.class);
            System.out.println(deleteResponseBody);
            assertThat(deleteResponseBody.getMeta().getCode(), equalTo(metaDelete.getCode()));
            assertThat(deleteResponseBody.getMeta().getMessage(), equalTo(metaDelete.getMessage()));
        }
    }

