package homework_8.api.rqrs.users;

import homework_8.api.rqrs.AbstractBaseTest;
import homework_8.api.rqrs.Endpoints;
import homework_8.api.rqrs.Meta;
import io.restassured.http.ContentType;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.samePropertyValuesAs;

public class UsersGoRestTestAbstract extends AbstractBaseTest {

    Meta metaPost = new Meta(true, 201, "A resource was successfully created in response to a POST request. The Location header contains the URL pointing to the newly created resource.");
    Meta metaPatch = new Meta(true, 200, "OK. Everything worked as expected.");
    Meta metaDelete = new Meta(true, 204, "The request was handled successfully and the response contains no body content.");
    Result resultPost1 = new Result("Oxana", "Rom", "female", "ox.rom.test04@example.net", "1-234-567", "active");
    Result resultPost2 = new Result("Alina", "May", "female", "al.may.test04@example.net", "2-234-567", "active");
    Result resultPost3 = new Result("Evgenii", "Leb", "male", "ev.leb.test04@example.net", "3-234-567", "active");
    Result resultPatch1 = new Result("Oxana", "Rom", "female", "ox.rom.test04.update@example.net", "1-234-567", "active");
    Result resultPatch2 = new Result("Alina", "May", "female", "al.may.test04.update@example.net", "2-234-567", "active");
    Result resultPatch3 = new Result("Evgenii", "Leb", "male", "ev.leb.test04.update@example.net", "3-234-567", "active");
    UserRequest userPost1 = new UserRequest("Oxana", "Rom", "female", "ox.rom.test04@example.net", "1-234-567", "active");
    UserRequest userPost2 = new UserRequest("Alina", "May", "female", "al.may.test04@example.net", "2-234-567", "active");
    UserRequest userPost3 = new UserRequest("Evgenii", "Leb", "male", "ev.leb.test04@example.net", "3-234-567", "active");
    UserRequest userPatch1 = new UserRequest("Oxana", "Rom", "female", "ox.rom.test04.update@example.net", "1-234-567", "active");
    UserRequest userPatch2 = new UserRequest("Alina", "May", "female", "al.may.test04.update@example.net", "2-234-567", "active");
    UserRequest userPatch3 = new UserRequest("Evgenii", "Leb", "male", "ev.leb.test04.update@example.net", "3-234-567", "active");
    UserResponse userPostResponse1 = new UserResponse(metaPost, resultPost1);
    UserResponse userPostResponse2 = new UserResponse(metaPost, resultPost2);
    UserResponse userPostResponse3 = new UserResponse(metaPost, resultPost3);
    UserResponse userPatchResponse1 = new UserResponse(metaPatch, resultPatch1);
    UserResponse userPatchResponse2 = new UserResponse(metaPatch, resultPatch2);
    UserResponse userPatchResponse3 = new UserResponse(metaPatch, resultPatch3);

    @DataProvider
    public Object[][] entityDataProvider() {
        return new Object[][]{
                {userPost1, userPostResponse1, userPatch1, userPatchResponse1},
                {userPost2, userPostResponse2, userPatch2, userPatchResponse2},
                {userPost3, userPostResponse3, userPatch3, userPatchResponse3}
        };
    }

    @Test(dataProvider = "entityDataProvider")
    public void userRequestEntityDrivenTestingTest(UserRequest postRequest, UserResponse postResponse, UserRequest patchRequest, UserResponse patchResponse) {
        UserResponse postResponseBody = given()
                .spec(rqSpec)
                .contentType(ContentType.JSON)
                .body(postRequest)
                .when()
                .post(Endpoints.USERS_ENDPOINT)
                .as(UserResponse.class);
        System.out.println(postResponseBody);
        //assertThat(postResponseBody.getResult().getEmail(), equalTo(postRequest.getEmail()));
        //assertThat(postResponseBody.getResult().getFirstName(), equalTo(postRequest.getFirstName()));
        //assertThat(postResponseBody.getResult().getLastName(), equalTo(postRequest.getLastName()));
        //assertThat(postResponseBody.getResult().getPhone(), equalTo(postRequest.getPhone()));
        //assertThat(postResponseBody.getResult().getPhone(), equalTo(postRequest.getPhone()));
        //assertThat(postResponseBody.getMeta(), equalTo(postResponse.getMeta()));
        //assertThat(postResponseBody.getResult(), equalTo(postResponse.getResult()));
        assertThat(postResponseBody, samePropertyValuesAs(postResponse));
        UserResponse patchResponseBody = given()
                .spec(rqSpec)
                .pathParam("id", postResponseBody.getResult().getId())
                .contentType(ContentType.JSON)
                .body(patchRequest)
                .when()
                .patch(Endpoints.PATCH_DELETE_USERS_ENDPOINT)
                .as(UserResponse.class);
        System.out.println(patchResponseBody);
        assertThat(patchResponseBody, samePropertyValuesAs(patchResponse));
        //assertThat(patchResponseBody.getMeta().getCode(), equalTo(metaPatch.getCode()));
        //assertThat(patchResponseBody.getMeta().getMessage(), equalTo(metaPatch.getMessage()));
        UserResponse deleteResponseBody = given()
                .spec(rqSpec)
                .pathParam("id", postResponseBody.getResult().getId())
                .contentType(ContentType.JSON)
                .body("")
                .when()
                .delete(Endpoints.PATCH_DELETE_USERS_ENDPOINT)
                .as(UserResponse.class);
        System.out.println(deleteResponseBody);
        assertThat(deleteResponseBody.getMeta().getCode(), equalTo(metaDelete.getCode()));
        assertThat(deleteResponseBody.getMeta().getMessage(), equalTo(metaDelete.getMessage()));
    }
}
