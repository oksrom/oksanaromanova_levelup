package homework_7.selenide.scripts.task_1_practice;

import com.codeborne.selenide.Browsers;
import com.codeborne.selenide.Configuration;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;

public class MailTestPracticeSelenide {

    @BeforeMethod
    public void setUp() {
        Configuration.browser = Browsers.CHROME;
        Configuration.timeout = 7500;
        Configuration.reportsFolder = "target/reports/tests";
        Configuration.startMaximized = true;
    }

    @Test
    public void selennidePractice() {
        open();
        $(By.id("mailbox:login")).sendKeys("oksana_automation_tests");
        $(By.id("mailbox:submit")).click();
        $(By.id("mailbox:password")).sendKeys("levelup2020");
        $(By.id("mailbox:submit")).click();
        sleep(3000);
        $x("//*[text() ='Написать письмо']").shouldBe(visible);
    }


    void sleep(long timeout) {
        try {
            Thread.sleep(timeout);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
