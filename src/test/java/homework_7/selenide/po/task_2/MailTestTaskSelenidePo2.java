package homework_7.selenide.po.task_2;

import homework_7.selenide.po.AbstractBaseTest;
import homework_7.selenide.po.ReadFile;
import org.testng.annotations.Test;

import java.util.Properties;
import java.util.Random;

import static homework_7.selenide.po.HomePage.*;
import static homework_7.selenide.po.MailPage.*;

public class MailTestTaskSelenidePo2 extends AbstractBaseTest {

    @Test
    public void seleniumTestPo2() {
        Random random = new Random();
        int uniqueId = random.nextInt(500);
        String subjectFill = "Тест " + uniqueId;
        String textFill = "This is the test e-mail!" + uniqueId;
        open();
        clickSubmitButton();
        Properties properties = ReadFile.readFromFile("src/main/resources/config.properties");
        String username = properties.getProperty("email.username");
        String password = properties.getProperty("email.password");
        String emailFill = properties.getProperty("email.address");
        login(username, password);
        mailPageOpened();
        //Assert, что вход выполнен успешно
        isWriteEmailDisplayed();

        writeNewEmail(emailFill, subjectFill, textFill);
        clickSendButton();
        clickCloseButton();
        enterSentFolder();

        //Verify, что письмо сохранено в отправленных - оно в первой строчке
       isLastEmailDisplayed();

        //Проверка, что это именно то письмо по теме
        doesSubjectTextContain(subjectFill);
        enterTestSubFolder();

        //проверка, что в папке Тест именно то письмо, а не другое по разным характеристикам (Verify контент, адресата и тему письма)
        isLastEmailDisplayed();
        doesSenderUnreadEmailContains(emailFill);
        isUnreadSubjectTheRightOne(subjectFill);
        isTextTheRightOne(textFill);

        logout();
    }
}
