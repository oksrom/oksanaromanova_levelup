package homework_7.selenide.po.task_1;

import homework_7.selenide.po.AbstractBaseTest;
import homework_7.selenide.po.ReadFile;
import org.testng.annotations.Test;

import java.util.Properties;
import java.util.Random;

import static homework_7.selenide.po.HomePage.*;
import static homework_7.selenide.po.MailPage.*;

public class MailTestTaskSelenidePo1 extends AbstractBaseTest {

    @Test
    public void selenideTestPo1() {
        Random random = new Random();
        int uniqueId = random.nextInt(500);
        String subjectFill = "Task1 " + uniqueId;
        String textFill = "This is the test e-mail!" + uniqueId;
        open();
        clickSubmitButton();
        Properties properties = ReadFile.readFromFile("src/main/resources/config.properties");
        String username = properties.getProperty("email.username");
        String password = properties.getProperty("email.password");
        String emailFill = properties.getProperty("email.address");
        login(username, password);
        mailPageOpened();
        //Assert, что вход выполнен успешно
        isWriteEmailDisplayed();
        writeNewEmail(emailFill, subjectFill, textFill);
        clickSaveButton();
        clickCloseButton();
        enterDraftsFolder();
        isLastEmailDisplayed();
        //проверка, что это именно тот, а не другой черновик по разным характеристикам (Verify контент, адресата и тему письма)
        isEmailTheRightOne(emailFill);
        isSubjectTheRightOne(subjectFill);
        isTextTheRightOne(textFill);
        clickOnLastEmail();
        clickSendButton();
        clickCloseButton();
        isItAnotherEmailBySubject(subjectFill);
        //Verify, что письмо появилось в папке отправленные. Проверка, что это именно тот, а не другой имейл по теме
        enterSentFolder();
        doesSubjectTextContain(subjectFill);

        logout();
    }
}
