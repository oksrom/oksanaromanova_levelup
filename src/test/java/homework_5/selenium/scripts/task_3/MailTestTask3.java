package homework_5.selenium.scripts.task_3;

import homework_5.selenium.scripts.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.util.Random;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class MailTestTask3 extends BaseTest {

    @Test
    public void seleniumTest3() {
        BaseTest baseTest = new BaseTest();
        Random random = new Random();
        int uniqueId = random.nextInt(500);
        String subjectFill = "Task3 " + uniqueId;
        String emailFill = "oksana_automation_tests@mail.ru";
        String textFill = "This is the test e-mail!" + uniqueId;
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mailbox:login"))).sendKeys("oksana_automation_tests");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mailbox:submit"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mailbox:password"))).sendKeys("levelup2020");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mailbox:submit"))).click();
        wait.until(ExpectedConditions.titleContains("Входящие"));
        WebElement writeEmail = driver.findElement(By.xpath(("//*[text() ='Написать письмо']")));
        wait.until(ExpectedConditions.visibilityOf(writeEmail));
        //Assert, что вход выполнен успешно
        assertTrue(writeEmail.isDisplayed());
        writeEmail.click();
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[data-type='to'] input"))).sendKeys(emailFill);
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[name='Subject']"))).sendKeys(subjectFill);
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[role='textbox']"))).sendKeys(textFill);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text() ='Отправить']"))).click();
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[title ='Закрыть']"))).click();
        WebElement tomyselfFolder = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@href='/tomyself/']")));
        tomyselfFolder.click();
        //Verify, что письмо появилось в папке письма себе - оно в первой строчке
        WebElement lastReceivedEmail = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("*//div[@class='llc__content']")));
        assertTrue(lastReceivedEmail.isDisplayed());
        //Проверка, что это именно то письмо (Verify контент, адресата и тему письма)
        WebElement sender = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("llc__item_unread")));
        assertTrue(sender.getText().contains(emailFill.substring(1,5)));
        WebElement subject = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class = 'llc__subject llc__subject_unread']")));
        assertEquals(subject.getText(), (subjectFill));
        WebElement text = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class = 'll-sp__normal']")));
        assertTrue(text.getText().contains(textFill));
        wait.until(ExpectedConditions.elementToBeClickable(lastReceivedEmail)).click();
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[title ='Удалить']"))).click();
        WebElement trash = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@href='/trash/']")));
        trash.click();
        //Verify, что письмо появилось в корзине - оно в первой строчке
        //sleep(2000);
        WebElement lastDeletedEmail = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("*//div[@class='llc__content']")));
        assertTrue(lastDeletedEmail.isDisplayed());
        //Проверка, что это именно то письмо по теме
        WebElement subjectOfDeletedEmail = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("*//span[@class='ll-sj__normal']")));
        assertEquals(subjectOfDeletedEmail.getText(), (subjectFill));
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@title='выход']"))).click();
    }

}
