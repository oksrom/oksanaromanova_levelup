package homework_5.selenium.scripts.task_2;

import homework_5.selenium.scripts.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.util.Random;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class MailTestTask2 extends BaseTest {

    @Test
    public void seleniumTest2(){
        BaseTest baseTest = new BaseTest();
        Random random = new Random();
        int uniqueId = random.nextInt(500);
        String subjectFill = "Тест " + uniqueId;
        String emailFill = "oksana_automation_tests@mail.ru";
        String textFill = "This is the test e-mail!" + uniqueId;
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mailbox:login"))).sendKeys("oksana_automation_tests");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mailbox:submit"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mailbox:password"))).sendKeys("levelup2020");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mailbox:submit"))).click();
        wait.until(ExpectedConditions.titleContains("Входящие"));
        WebElement writeEmail = driver.findElement(By.xpath(("//*[text() ='Написать письмо']")));
        wait.until(ExpectedConditions.visibilityOf(writeEmail));
        //Assert, что вход выполнен успешно
        assertTrue(writeEmail.isDisplayed());
        writeEmail.click();
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[data-type='to'] input"))).sendKeys(emailFill);
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[name='Subject']"))).sendKeys(subjectFill);
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[role='textbox']"))).sendKeys(textFill);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text() ='Отправить']"))).click();
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[title ='Закрыть']"))).click();
        WebElement sentFolder = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@href='/sent/']")));
        sentFolder.click();
        //Verify, что письмо сохранено в отправленных - оно в первой строчке
        WebElement lastSentEmail = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("*//div[@class='llc__content']")));
        assertTrue(lastSentEmail.isDisplayed());
        //Проверка, что это именно то письмо по теме
        assertTrue(lastSentEmail.getText().contains(subjectFill));
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='nav__folder-name']/div[text()='Тест']"))).click();
        //проверка, что в папке Тест именно то письмо, а не другое по разным характеристикам (Verify контент, адресата и тему письма)
        WebElement sender = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("llc__item_unread")));
        assertTrue(sender.getText().contains(emailFill.substring(1,5)));
        WebElement subject = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class = 'llc__subject llc__subject_unread']")));
        assertEquals(subject.getText(), (subjectFill));
        WebElement text = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class = 'll-sp__normal']")));
        assertTrue(text.getText().contains(textFill));
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@title='выход']"))).click();
    }

}
