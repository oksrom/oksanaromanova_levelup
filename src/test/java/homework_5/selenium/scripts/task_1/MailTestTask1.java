package homework_5.selenium.scripts.task_1;

import homework_5.selenium.scripts.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.util.Random;

import static org.testng.Assert.*;

public class MailTestTask1 extends BaseTest {

    @Test
    public void seleniumTest1() {
        BaseTest baseTest = new BaseTest();
        Random random = new Random();
        int uniqueId = random.nextInt(500);
        String subjectFill = "Task1 " + uniqueId;
        String emailFill = "oksana_automation_tests@mail.ru";
        String textFill = "This is the test e-mail!" + uniqueId;
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mailbox:login"))).sendKeys("oksana_automation_tests");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mailbox:submit"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mailbox:password"))).sendKeys("levelup2020");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mailbox:submit"))).click();
        wait.until(ExpectedConditions.titleContains("Входящие"));
        WebElement writeEmail = driver.findElement(By.xpath(("//*[text() ='Написать письмо']")));
        wait.until(ExpectedConditions.visibilityOf(writeEmail));
        //Assert, что вход выполнен успешно
        assertTrue(writeEmail.isDisplayed());
        writeEmail.click();
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[data-type='to'] input"))).sendKeys(emailFill);
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[name='Subject']"))).sendKeys(subjectFill);
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[role='textbox']"))).sendKeys(textFill);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text() ='Сохранить']"))).click();
        //wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//[contains(text(),'Сохранено в черновиках') and @class='notify__message__text']")));
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[title ='Закрыть']"))).click();
        WebElement draftFolder = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@href='/drafts/']")));
        draftFolder.click();
        //sleep(5000);
        //Verify, что письмо сохранено в черновиках - он в первой строчке
        WebElement lastDraftEmail = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("*//div[@class='llc__content']")));
        assertTrue(lastDraftEmail.isDisplayed());
        WebElement sender = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class = 'llc__item llc__item_correspondent']")));
        //проверка, что это именно тот, а не другой черновик по разным характеристикам (Verify контент, адресата и тему письма)
        assertEquals(sender.getText(), (emailFill));
        WebElement subject = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class = 'llc__subject']")));
        assertEquals(subject.getText(), (subjectFill));
        WebElement text = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class = 'll-sp__normal']")));
        assertTrue(text.getText().contains(textFill));
        wait.until(ExpectedConditions.elementToBeClickable(lastDraftEmail)).click();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text() ='Отправить']"))).click();
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[title ='Закрыть']"))).click();
        //Precondition: в папке черновики уже были черновики. проверка, что последнее сохраненное в черновиках письмо с др темой (Verify, что письмо исчезло из черновиков)
        //Если в папке черновики не было черновиков (она осталась пустой после отправки):
        /*
        WebElement draftFolderIsEmpty = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("[class='octopus__title']")));
        assertEquals(draftFolderIsEmpty.getText(), "У вас нет незаконченных\n" +
                "или неотправленных писем");

         */
        //sleep(2000); //слип добавлен, т.к. тест бывает не стабилен и падает с StaleElementReferenceException: stale element reference: element is not attached to the page document (устаревание айди?) - как бороться?
        WebElement lastSubjectInDrafts = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class = 'llc__subject']")));
        assertNotEquals(lastSubjectInDrafts.getText(), (subjectFill));
        //Verify, что письмо появилось в папке отправленные. Проверка, что это именно тот, а не другой имейл по теме
        WebElement sentFolder = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@href='/sent/']")));
        sentFolder.click();
        //sleep(2000);// см.коммент выше
        WebElement lastSubjectInSent = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class = 'll-sj__normal']")));
        assertTrue(lastSubjectInSent.getText().contains(subjectFill));
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@title='выход']"))).click();
    }

}
