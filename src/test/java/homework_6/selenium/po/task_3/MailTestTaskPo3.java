package homework_6.selenium.po.task_3;

import homework_6.selenium.po.AbstractBaseTest;
import homework_6.selenium.po.HomePage;
import homework_6.selenium.po.MailPage;
import org.testng.annotations.Test;

import java.util.Properties;
import java.util.Random;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class MailTestTaskPo3 extends AbstractBaseTest {

    @Test
    public void seleniumTestPo3() {
        Random random = new Random();
        int uniqueId = random.nextInt(500);
        String subjectFill = "Task3 " + uniqueId;
        String textFill = "This is the test e-mail!" + uniqueId;
        HomePage homePage = new HomePage(driver);
        homePage.open();
        homePage.clickSubmitButton();
        Properties property = new Properties();
        homePage.readFromFile(property);
        String username = property.getProperty("email.username");
        String password = property.getProperty("email.password");
        String emailFill = property.getProperty("email.address");
        homePage.login(username, password);
        MailPage mailPage = new MailPage(driver);
        //Assert, что вход выполнен успешно
        assertTrue(mailPage.isMailPageOpened());
        assertTrue(mailPage.isWriteEmailDisplayed());
        mailPage.writeNewEmail(emailFill, subjectFill, textFill);
        mailPage.clickSendButton();
        mailPage.clickCloseButton();
        mailPage.enterTomyselfFolder();
        //Verify, что письмо появилось в папке письма себе - оно в первой строчке
        assertTrue(mailPage.isLastEmailDisplayed(), "Письмо не сохранено в папке Письма себе");

        //Проверка, что это именно то письмо (Verify контент, адресата и тему письма)
        assertTrue(mailPage.emailTextUnreadEmailContains(emailFill.substring(1, 5)));

        assertEquals(mailPage.getSubjectTextUnreadEmail(), subjectFill);
        assertEquals(mailPage.getBodyText(), textFill);
        mailPage.clickOnLastEmail();
        mailPage.clickDeleteButton();
        mailPage.enterTrashFolder();
        //Verify, что письмо появилось в корзине - оно в первой строчке
        mailPage.isLastEmailDisplayed();
        //Проверка, что это именно то письмо по теме
        assertEquals(mailPage.getSubjectText(), subjectFill);
        mailPage.logout();
    }
}
