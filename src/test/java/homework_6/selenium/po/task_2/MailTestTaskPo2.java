package homework_6.selenium.po.task_2;

import homework_6.selenium.po.AbstractBaseTest;
import homework_6.selenium.po.HomePage;
import homework_6.selenium.po.MailPage;
import org.testng.annotations.Test;

import java.util.Properties;
import java.util.Random;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class MailTestTaskPo2 extends AbstractBaseTest {

    @Test
    public void seleniumTestPo2() {
        Random random = new Random();
        int uniqueId = random.nextInt(500);
        String subjectFill = "Тест " + uniqueId;
        String textFill = "This is the test e-mail!" + uniqueId;
        HomePage homePage = new HomePage(driver);
        homePage.open();
        homePage.clickSubmitButton();
        Properties property = new Properties();
        homePage.readFromFile(property);
        String username = property.getProperty("email.username");
        String password = property.getProperty("email.password");
        String emailFill = property.getProperty("email.address");
        homePage.login(username, password);
        MailPage mailPage = new MailPage(driver);
        //Assert, что вход выполнен успешно
        assertTrue(mailPage.isMailPageOpened());
        assertTrue(mailPage.isWriteEmailDisplayed());

        mailPage.writeNewEmail(emailFill, subjectFill, textFill);
        mailPage.clickSendButton();
        mailPage.clickCloseButton();
        mailPage.enterSentFolder();
        //Verify, что письмо сохранено в отправленных - оно в первой строчке
        assertTrue(mailPage.isLastEmailDisplayed(), "Письмо не сохранено в отправленных");

        //Проверка, что это именно то письмо по теме
        assertTrue(mailPage.subjectTextContains(subjectFill));
        mailPage.enterTestSubFolder();
        //проверка, что в папке Тест именно то письмо, а не другое по разным характеристикам (Verify контент, адресата и тему письма)
        assertTrue(mailPage.isLastEmailDisplayed(), "Письмо не сохранено в папке Тест");

        assertTrue(mailPage.emailTextUnreadEmailContains(emailFill.substring(1, 5)));

        assertEquals(mailPage.getSubjectTextUnreadEmail(), subjectFill);
        assertEquals(mailPage.getBodyText(), textFill);
        mailPage.logout();
    }
}
