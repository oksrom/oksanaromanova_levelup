package homework_6.selenium.po.task_1;

import homework_6.selenium.po.AbstractBaseTest;
import homework_6.selenium.po.HomePage;
import homework_6.selenium.po.MailPage;
import org.testng.annotations.Test;

import java.util.Properties;
import java.util.Random;

import static org.testng.Assert.*;

public class MailTestTaskPo1 extends AbstractBaseTest {

    @Test
    public void seleniumTestPo1() {
        Random random = new Random();
        int uniqueId = random.nextInt(500);
        String subjectFill = "Task1 " + uniqueId;
        String textFill = "This is the test e-mail!" + uniqueId;
        HomePage homePage = new HomePage(driver);
        homePage.open();
        homePage.clickSubmitButton();
        Properties property = new Properties();
        homePage.readFromFile(property);
        String username = property.getProperty("email.username");
        String password = property.getProperty("email.password");
        String emailFill = property.getProperty("email.address");
        homePage.login(username, password);
        MailPage mailPage = new MailPage(driver);
        //Assert, что вход выполнен успешно
        assertTrue(mailPage.isMailPageOpened());
        assertTrue(mailPage.isWriteEmailDisplayed());
        mailPage.writeNewEmail(emailFill, subjectFill, textFill);
        mailPage.clickSaveButton();
        mailPage.clickCloseButton();
        mailPage.enterDraftsFolder();
        //Verify, что письмо сохранено в черновиках - он в первой строчке
        assertTrue(mailPage.isLastEmailDisplayed(), "Письмо не сохранено в черновиках");
        //проверка, что это именно тот, а не другой черновик по разным характеристикам (Verify контент, адресата и тему письма)
        assertEquals(mailPage.getEmailText(), property.getProperty("email.address"));
        assertEquals(mailPage.getSubjectText(), subjectFill);
        assertEquals(mailPage.getBodyText(), textFill);
        mailPage.clickOnLastEmail();
        mailPage.clickSendButton();
        mailPage.clickCloseButton();
        assertNotEquals(mailPage.getSubjectText(), subjectFill);
        //Verify, что письмо появилось в папке отправленные. Проверка, что это именно тот, а не другой имейл по теме
        mailPage.enterSentFolder();
        assertTrue(mailPage.getSubjectText().contains(subjectFill));
        assertTrue(mailPage.subjectTextContains(subjectFill));

        mailPage.logout();
    }
}
