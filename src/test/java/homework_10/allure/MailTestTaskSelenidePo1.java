package homework_10.allure;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.logevents.SelenideLogger;
import homework_10.allure.po.ReadFile;
import io.qameta.allure.Attachment;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import io.qameta.allure.selenide.AllureSelenide;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.util.Properties;
import java.util.Random;

import static com.codeborne.selenide.Selenide.title;
import static homework_10.allure.po.HomePage.*;
import static homework_10.allure.po.MailPage.*;

@Feature("Selenide Alure")
public class MailTestTaskSelenidePo1 extends AbstractBaseTest {

    @BeforeSuite
    public void beforeSuite() {
        SelenideLogger.addListener("AllureSelenide",
                new AllureSelenide().savePageSource(true).screenshots(true));
        Configuration.headless=true;

        System.out.println();
        System.out.println(System.getenv("KEY_UI_TEST"));
        System.out.println();
    }

    @Test
    @Story("HomePage Test")
    @TmsLink("TS-01")
    public void selenideTestPoHomePage() {
        open();
        isSubmitButtonDisplayed();
        getPageTitle();
        Properties properties = ReadFile.readFromFile("src/main/resources/config.properties");
        String username = properties.getProperty("email.username");
        String password = properties.getProperty("email.password");
        login(username, password);
    }

    @Test
    @Story("MailPage Test")
    @TmsLink("TS-02")
    public void selenideTestPoMailPage() {
        Random random = new Random();
        int uniqueId = random.nextInt(500);
        String subjectFill = "Task1 " + uniqueId;
        String textFill = "This is the test e-mail!" + uniqueId;
        open();
        isSubmitButtonDisplayed();
        Properties properties = ReadFile.readFromFile("src/main/resources/config.properties");
        String emailFill = properties.getProperty("email.address");
        mailPageOpened();
        //Assert, что вход выполнен успешно
        isWriteEmailDisplayed();
        writeNewEmail(emailFill, subjectFill, textFill);
        clickSaveButton();
        clickCloseButton();
        enterDraftsFolder();
        isLastEmailDisplayed();
        //проверка, что это именно тот, а не другой черновик по разным характеристикам (Verify контент, адресата и тему письма)
        isEmailTheRightOne(emailFill);
        isSubjectTheRightOne(subjectFill);
        isTextTheRightOne(textFill);
        clickOnLastEmail();
        clickSendButton();
        clickCloseButton();
        isItAnotherEmailBySubject(subjectFill);
        //Verify, что письмо появилось в папке отправленные. Проверка, что это именно тот, а не другой имейл по теме
        enterSentFolder();
        doesSubjectTextContain(subjectFill);
        logout();
    }

    @Attachment(value = "Page Title", type = "plain/text", fileExtension = "txt")
    public String getPageTitle() {
        return title();
    }
}
