package homework_8.api.rqrs;

import java.util.Objects;

public class Meta {

    private boolean success;
    private int code;
    private String message;

    public Meta(boolean success, int code, String message) {
        this.success = success;
        this.code = code;
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "Meta{" +
                "success=" + success +
                ", code=" + code +
                ", message='" + message + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Meta meta = (Meta) o;
        return success == meta.success &&
                code == meta.code &&
                Objects.equals(message, meta.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(success, code, message);
    }
}
