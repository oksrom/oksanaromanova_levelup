package homework_8.api.rqrs.users;

import homework_8.api.rqrs.Meta;
import homework_8.api.rqrs.MetaResponse;

import java.util.Objects;

public class UserResponse extends MetaResponse {

    private Result result;

    public UserResponse(Meta meta, Result result) {
        this.meta = meta;
        this.result = result;
    }

    public Meta getMeta() {
        return meta;
    }

    public Result getResult() {
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserResponse that = (UserResponse) o;
        return Objects.equals(meta, that.meta) &&
                Objects.equals(result, that.result);
    }

    @Override
    public int hashCode() {
        return Objects.hash(meta, result);
    }

    @Override
    public String toString() {
        return "UserResponse{" +
                "_meta=" + meta +
                ", result=" + result +
                '}';
    }
}