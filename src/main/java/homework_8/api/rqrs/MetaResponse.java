package homework_8.api.rqrs;

import com.google.gson.annotations.SerializedName;

public class MetaResponse {
    @SerializedName("_meta")
    public Meta meta;
}
