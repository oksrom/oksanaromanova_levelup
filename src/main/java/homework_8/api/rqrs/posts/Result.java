package homework_8.api.rqrs.posts;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class Result {

    @SerializedName("user_id")
    private int userId;

    private String title;
    private String body;
    private long id;

    public Result(int userId, String title, String body) {
        this.userId = userId;
        this.title = title;
        this.body = body;
    }

    public long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Result result = (Result) o;
        return userId == result.userId &&
                Objects.equals(title, result.title) &&
                Objects.equals(body, result.body);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, title, body);
    }

    @Override
    public String toString() {
        return "Result{" +
                "userId=" + userId +
                ", title='" + title + '\'' +
                ", body='" + body + '\'' +
                '}';
    }
}
