package homework_8.api.rqrs.posts;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class PostsRequest {

    @SerializedName("user_id")
    private int userId;
    private String title;
    private String body;

    public PostsRequest(int userId, String title, String body) {
        this.userId = userId;
        this.title = title;
        this.body = body;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PostsRequest that = (PostsRequest) o;
        return userId == that.userId &&
                Objects.equals(title, that.title) &&
                Objects.equals(body, that.body);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, title, body);
    }

    @Override
    public String toString() {
        return "PostsRequest{" +
                "userId=" + userId +
                ", title='" + title + '\'' +
                ", body='" + body + '\'' +
                '}';
    }
}

