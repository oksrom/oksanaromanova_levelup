package homework_8.api.rqrs.posts;

import homework_8.api.rqrs.Meta;
import homework_8.api.rqrs.MetaResponse;

import java.util.Objects;

public class PostsResponse extends MetaResponse {

    private Result result;

    public PostsResponse(Meta meta, Result result) {
        this.meta = meta;
        this.result = result;
    }

    public Meta getMeta() {
        return meta;
    }

    public Result getResult() {
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PostsResponse that = (PostsResponse) o;
        return Objects.equals(meta, that.meta) &&
                Objects.equals(result, that.result);
    }

    @Override
    public int hashCode() {
        return Objects.hash(meta, result);
    }

    @Override
    public String toString() {
        return "UserResponse{" +
                "_meta=" + meta +
                ", result=" + result +
                '}';
    }
}