package homework_8.api.rqrs.comments;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class Result {

    @SerializedName("post_id")
    private int postId;
    private long id;
    private String name;
    private String email;
    private String body;

    public Result(int postId, String name, String email, String body) {
        this.postId = postId;
        this.name = name;
        this.email = email;
        this.body = body;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Result that = (Result) o;
        return postId == that.postId &&
                Objects.equals(name, that.name) &&
                Objects.equals(email, that.email) &&
                Objects.equals(body, that.body);
    }

    @Override
    public int hashCode() {
        return Objects.hash(postId, name, email, body);
    }

    @Override
    public String toString() {
        return "Result{" +
                "postId=" + postId +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", body='" + body + '\'' +
                '}';
    }
}
