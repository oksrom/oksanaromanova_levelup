package homework_6.selenium.po;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public abstract class AbstractBasePage {
    protected WebDriver driver;
    protected WebDriverWait wait;

    public AbstractBasePage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(driver, this);
    }

    public void readFromFile(Properties property) {
        try {
            FileInputStream fis = new FileInputStream("src/main/resources/config.properties");
            property.load(fis);
        } catch (
                IOException e) {
            System.err.println("Error: Property file is missing!");
        }
    }

    protected void elementClick(WebElement element) {
        wait.until(elementToBeClickable(element)).click();
    }

    protected void sendKeysToElement(WebElement element, String text) {
        WebElement textField = wait.until(visibilityOf(element));
        textField.clear();
        textField.sendKeys(text);
    }
}
