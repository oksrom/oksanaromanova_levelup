package homework_6.selenium.po;

import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class MailPage extends AbstractBasePage {

    @FindBy(xpath = "//*[text() ='Написать письмо']")
    private WebElement writeNewEmail;

    @FindBy(css = "[data-type='to'] input")
    private WebElement emailFill;

    @FindBy(name = "Subject")
    private WebElement subjectFill;

    @FindBy(css = "[role='textbox']")
    private WebElement textFill;

    @FindBy(xpath = "//*[text() ='Сохранить']")
    private WebElement saveButton;

    @FindBy(css = "[title ='Закрыть']")
    private WebElement closeButton;

    @FindBy(xpath = "//*[text() ='Отправить']")
    private WebElement sendButton;

    @FindBy(css = "[title ='Удалить']")
    private WebElement deleteButton;

    @FindBy(xpath = "//a[@title='выход']")
    private WebElement logoutButton;

    @FindBy(xpath = "//*[@href='/drafts/']")
    private WebElement draftsFolder;

    @FindBy(xpath = "//*[@href='/sent/']")
    private WebElement sentFolder;

    @FindBy(xpath = "//div[@class='nav__folder-name']/div[text()='Тест']")
    private WebElement testSubFolder;

    @FindBy(xpath = "//*[@href='/tomyself/']")
    private WebElement tomyselfFolder;

    @FindBy(xpath = "//*[@href='/trash/']")
    private WebElement trashFolder;

    @FindBy(xpath = "*//div[@class='llc__content']")
    private WebElement lastEmailInList;

    @FindBy(xpath = "//div[@class = 'llc__item llc__item_correspondent']")
    private WebElement sender;

    @FindBy(xpath = "//div[@class ='llc__item llc__item_correspondent llc__item_unread']")
    private WebElement senderUnread;

    @FindBy(xpath = "//span[@class = 'llc__subject']")
    private WebElement subject;

    @FindBy(xpath = "//span[@class = 'llc__subject llc__subject_unread']")
    private WebElement subjectUnread;

    @FindBy(xpath = "//span[@class = 'll-sp__normal']")
    private WebElement text;

    public MailPage(WebDriver driver) {
        super(driver);
    }

    public void writeNewEmail(String email, String subject, String text) {
        elementClick(writeNewEmail);
        sendKeysToElement(emailFill, email);
        sendKeysToElement(subjectFill, subject);
        sendKeysToElement(textFill, text);
    }

    public void clickSaveButton() {
        elementClick(saveButton);
    }

    public void clickCloseButton() {
        elementClick(closeButton);
    }

    public void clickSendButton() {
        elementClick(sendButton);
    }

    public void clickDeleteButton() {
        elementClick(deleteButton);
    }

    public void enterDraftsFolder() {
        elementClick(draftsFolder);
    }

    public void enterSentFolder() {
        elementClick(sentFolder);
    }

    public void enterTomyselfFolder() {
        elementClick(tomyselfFolder);
    }

    public void enterTestSubFolder() {
        elementClick(testSubFolder);
    }

    public void enterTrashFolder() {
        elementClick(trashFolder);
    }

    public void logout() {
        elementClick(logoutButton);
    }

    public boolean isWriteEmailDisplayed() {
        return wait.until(elementToBeClickable(writeNewEmail)).isDisplayed();
    }

    public boolean isLastEmailDisplayed() {
        try {
            return wait.until(elementToBeClickable(lastEmailInList)).isDisplayed();
        }
       catch (StaleElementReferenceException e) {
           return wait.until(elementToBeClickable(lastEmailInList)).isDisplayed();
       }
    }

    public void clickOnLastEmail() {
        elementClick(lastEmailInList);
    }

    public String getEmailText() {
        return wait.until(visibilityOf(sender)).getText();
    }

    public Boolean emailTextUnreadEmailContains(String string) {
        try{
            return wait.until(visibilityOf(senderUnread)).getText().contains(string);
        } catch (StaleElementReferenceException e) {
            return wait.until(visibilityOf(senderUnread)).getText().contains(string);
        }
    }

    public String getSubjectText() {
        try {
            return wait.until(visibilityOf(subject)).getText();
        }
        catch (StaleElementReferenceException e) {
            return wait.until(visibilityOf(subject)).getText();
        }
    }

    public Boolean subjectTextContains(String string) {
        return wait.until(visibilityOf(subject)).getText().contains(string);
    }

    public String getSubjectTextUnreadEmail() {
        return wait.until(visibilityOf(subjectUnread)).getText();
    }

    public String getBodyText() {
        return wait.until(visibilityOf(text)).getText();
    }

    public Boolean isMailPageOpened() {
        return wait.until(ExpectedConditions.titleContains("Входящие")).booleanValue();
    }
}
