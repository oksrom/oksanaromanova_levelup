package homework_6.selenium.po;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends AbstractBasePage {
    private static final String URL = "https://mail.ru";

    @FindBy(id = "mailbox:submit")
    private WebElement submitButton;

    @FindBy(id = "mailbox:login")
    private WebElement usernameTextField;

    @FindBy(id = "mailbox:password")
    private WebElement passwordTextField;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        driver.get(URL);
    }

    public void clickSubmitButton() {
        elementClick(submitButton);
    }

    public void login(String username, String password) {
        sendKeysToElement(usernameTextField, username);
        elementClick(submitButton);
        sendKeysToElement(passwordTextField, password);
        elementClick(submitButton);
    }

}
