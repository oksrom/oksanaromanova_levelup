package homework_10.allure.po;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.enabled;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class HomePage {
    private static final String URL = "https://mail.ru";

    @Step("Открывается домашняя страница 'https://mail.ru'")
    public static void open() {
        Selenide.open(URL);
    }

    @Step("Ввести имейл и пароль")
    public static void login(String username, String password) {
        $(By.id("mailbox:login-input")).sendKeys(username);
        $(By.id("mailbox:submit-button")).click();
        $(By.id("mailbox:password-input")).sendKeys(password);
        $(By.id("mailbox:submit-button")).click();
    }

    @Step("Страница для входа должна отобразиться")
    public static void isSubmitButtonDisplayed() {
        $(By.id("mailbox:submit-button")).shouldBe(enabled);
    }
}
