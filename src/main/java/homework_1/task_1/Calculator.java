package homework_1.task_1;

import java.util.Scanner;

public class Calculator {
        public static void main(String[] args) {
            Calculator calc = new Calculator();
            calc.startCalculation();
        }

    private void printResult(double result) {
        System.out.println(result);
    }

    Addition addition = new Addition();
    Subtraction subtraction = new Subtraction();
    Multiplication multiplication = new Multiplication();
    Exponentiation exponentiation = new Exponentiation();
    Factorial factorial = new Factorial();
    Fibonacci fibonacci = new Fibonacci();

    private void startCalculation() {
        System.out.println("Список доступных операций: 1 - сложение, 2 - вычитание, 3 - умножение, 4 - возведение в степень, 5 - факториал, 6 - число Фибоначчи под Вашим номером");
        Scanner sc = new Scanner(System.in);
        System.out.println("Выберите оперпцию:");
        int operation = sc.nextInt();
        if (operation >= 1 && operation < 5) {
            System.out.println("Введите первое число:");
            double number1 = sc.nextDouble();
            System.out.println("Введите второе число:");
            double number2 = sc.nextDouble();
            switch (operation) {
                case 1:
                    printResult(addition.addition(number1, number2));
                    break;
                case 2:
                    printResult(subtraction.subtraction(number1, number2));
                    break;
                case 3:
                    printResult(multiplication.multiplication(number1, number2));
                    break;
                case 4:
                    printResult(exponentiation.exponentiation((int) number1, (int) number2));
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + operation);
            }
        } else {
            System.out.println("Введите число:");
            double number1 = sc.nextDouble();
            switch (operation) {
                case 5:
                    printResult(factorial.factorial((int) number1));
                    break;
                case 6:
                    printResult(fibonacci.fibonacci((int) number1));
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + operation);
            }
        }
    }
}

