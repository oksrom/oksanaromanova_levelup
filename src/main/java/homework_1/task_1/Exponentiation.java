package homework_1.task_1;

public class Exponentiation {

    public int exponentiation(int i, int j) {
        double result = 1;
        if (j < 0) {
            for (int k = 1; k <= (-j); k++) {
                result = 1/((1/result)*i);
            }
        } else if (j > 0) {
            for (int k = 1; k <= j; k++) {
                result *= i;
            }
        }
        return (int) result;
    }
}
