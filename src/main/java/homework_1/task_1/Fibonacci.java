package homework_1.task_1;

public class Fibonacci {
// Методом цикла
    public long fibonacci(int i) {
        int a = 0;
        int b;
        int result = 1;
        if (i < 1) {
            System.out.print("Error input");
            return i;
        }
        if (i == 1) {
            return 0;
        }
        if (i == 2 || i == 3) {
            return 1;
        }
        else {
            for (int k = 1; k <= i - 2; k++) {
                b = result;
                result += a;
                a = b;
            }
           return result;
    }

   /* Методом рекурсии
   public long fibonacci(int i) {
        if (i < 1) {
            System.out.print("Error input");
            return i;
        }
        if (i==1)
            return 0;
        if (i == 2 || i == 3) {
            return 1;
        } else {
            return fibonacci (i - 2) + fibonacci (i - 1);
        }

    */
    }
}

