package homework_1.task_1;

public class Factorial {
    public int factorial(int i) {
        int factorial_result = 1;
        for (int k = 1; k <=i; k ++){
            factorial_result *= k;
        }
        return factorial_result;
    }
    /* Методом рекурсии
   public int factorial(int i) {
        if (i == 1 || i == 0) {
            return 1;
        } else {
            return i * factorial(i - 1);
        }
    }

     */
}
