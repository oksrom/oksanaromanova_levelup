package homework_2.task_1;

public class Bus extends Vehicle implements Rideable {
    boolean social;

    Bus(String type, Double fuelConsumptionPer100km, String brand, int passengerSeats, double price, boolean social) {
        super(type, fuelConsumptionPer100km, brand, passengerSeats, price);
        this.social = social;
    }

    @Override
    void printOptions () {
        System.out.println("Brand of bus:" + brand+ " Fuel consumption per 100km:" + fuelConsumptionPer100km + " Amount of passenger seats:" + passengerSeats + isSocial (social));
    }

    boolean isSocial(boolean social) {
        return this.social;
    }

    public void ride() {
        System.out.println(" Ride is not fast, but safe");
    }
}
