package homework_2.task_1;

public class Marshrutka extends Vehicle implements Rideable {
    boolean conditioner;

    Marshrutka(String type, Double fuelConsumptionPer100km, String brand, int passengerSeats, double price, boolean conditioner) {
        super(type, fuelConsumptionPer100km, brand, passengerSeats, price);
        this.conditioner = conditioner;
    }


    @Override
    void printOptions () {
        System.out.println("Model of marshrutka:" + brand + " Fuel consumption per 100km:" + fuelConsumptionPer100km + "Amount of passenger seats:" + passengerSeats + conditionerIsThere (conditioner));
    }

    boolean conditionerIsThere (boolean conditioner) {
        return conditioner;
    }

    public void ride() {
        System.out.println(" Ride is fast and furious");
    }
}
