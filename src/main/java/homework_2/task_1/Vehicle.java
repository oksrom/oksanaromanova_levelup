package homework_2.task_1;

public class Vehicle implements Comparable<Vehicle> {
    String type;
    Double fuelConsumptionPer100km;
    double price;
    int passengerSeats;
    String brand;
    static Vehicle[] vehicles;

    Vehicle (String type, Double fuelConsumptionPer100km, String brand, int passengerSeats, double price) {
        this.type = type;
        this.fuelConsumptionPer100km = fuelConsumptionPer100km;
        this.brand = brand;
        this.passengerSeats = passengerSeats;
        this.price = price;
    }

    public static void setVehicles(Vehicle[] vehicles) {
        Vehicle.vehicles = vehicles;
    }

    public int getPassengerSeats() {
        return passengerSeats;
    }

    void printOptions () {
        System.out.println("Model: " + brand + " Fuel consumption per 100km: " + fuelConsumptionPer100km + " Amount of passenger seats: " + passengerSeats + " Price: " + price);
    }

    @Override
    public int compareTo(Vehicle v1) {
        int result = this.fuelConsumptionPer100km.compareTo(v1.fuelConsumptionPer100km);
        if (result == 0) {
            result = this.fuelConsumptionPer100km.compareTo(v1.fuelConsumptionPer100km);
        }
        return result;
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "type of transport ='" + type + '\'' +
                ", model ='" + brand + '\'' +
                ", fuelConsumption =" + fuelConsumptionPer100km +
                '}';
    }
}
