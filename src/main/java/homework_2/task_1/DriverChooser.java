package homework_2.task_1;

public class DriverChooser {
    private String license;
    String name;

    DriverChooser(String name, String license) {
        this.name = name;
        this.license = license;
    }

    public String getLicense() {
        return license;
    }
}
