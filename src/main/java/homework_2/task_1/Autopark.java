package homework_2.task_1;

import java.io.*;
import java.util.*;

public class Autopark {
    public static void main(String[] args) throws IOException {
        Autopark autopark = new Autopark();
        System.out.println("Availabilities for drivers:");
        autopark.availableVehicles(autopark.person1);
        System.out.println("=========================");
        autopark.availableVehicles(autopark.person2);
        System.out.println("=========================");
        autopark.priceAutopark();
        System.out.println("=========================");
        autopark.vehiclesSet();
        System.out.println("=========================");
        System.out.println("Checked and unchecked ones");
        Autopark exception = new Autopark();
        File file = new File("C:\\Users\\oromanov\\Documents\\T-M@GIC\\GIT\\OksanaRomanova_LevelUp\\src\\files.txt");
        exception.readFromFile(file);
        Set<Vehicle> vehicleSet = new TreeSet<Vehicle>();
        vehicleSet.add(null);
        System.out.println("=========================");
        System.out.println("Unchecked exceptions:");
        autopark.priceAutoparkWithUncheckedException();
    }

    Bus volzhanin = new Bus("bus", 29.8, "Volzhanin", 72, 1.4, true);
    Bus paz = new Bus("bus", 20.5, "PAZ", 25, 0.87, false);
    Bus hyundai = new Bus("bus", 34.7, "Hyundai", 90, 7.7, true);
    Bus[] buses = {volzhanin, paz, hyundai};

    Marshrutka gaz = new Marshrutka("marshrutka", 17.5, "GAZ", 25, 0.87, false);
    Marshrutka mercedes = new Marshrutka("marshrutka", 9.6, "Mercedes", 19, 1.5, true);
    Marshrutka[] marshrutkas = {gaz, mercedes};

    Vehicle[] vehicles = {volzhanin, paz, hyundai, gaz, mercedes};

    DriverChooser person1 = new DriverChooser("Michael", "B");
    DriverChooser person2 = new DriverChooser("Bob", "BC");

    public void availableVehicles(DriverChooser person) {
        System.out.println(person.name + ", with your driving license available: ");
        if (person.getLicense().equals("BC")) {
            for (Bus bus : buses
            ) {
                bus.printOptions();
                bus.ride();
            }
        }
        for (Marshrutka marshrutka : marshrutkas
        ) {
            marshrutka.printOptions();
            marshrutka.ride();
        }
    }

      public void priceAutopark() {
        double priceMarshrutkas = 0;
        double priceBuses = 0;
        double priceAutopark;
        for (Marshrutka marshrutka : marshrutkas
        ) {
            priceMarshrutkas += marshrutka.price;
        }
        for (Bus bus : buses
        ) {
            priceBuses += bus.price;
        }
        priceAutopark = priceBuses + priceMarshrutkas;
        System.out.println("Price of autopark in mln rub: " + priceAutopark);
    }

    public void priceAutoparkWithUncheckedException () {
        double priceMarshrutkas = 0;
        double priceBuses = 0;
        double priceAutopark;
        for (int i = 0; i <= marshrutkas.length; i++) {
            priceMarshrutkas += marshrutkas[i].price;
        }
        for (int j = 0; j < buses.length; j++)
        {
            priceBuses += buses[j].price;
        }
        priceAutopark = priceBuses + priceMarshrutkas;
        System.out.println("Price of autopark in mln rub: " + priceAutopark);
    }

    public void vehiclesSet() {
        Set<Vehicle> vehicleSet = new TreeSet<Vehicle>();
        vehicleSet.addAll (Arrays.asList(vehicles));
        System.out.println("Sorted transport by fuel consumption");
        printSetVehicles(vehicleSet);
        findVehiclebyPassengerSeats(vehicleSet);
        System.out.println();
    }

    public void findVehiclebyPassengerSeats (Set<Vehicle> set) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Input requested min number of passenger seats:");
        int number = sc.nextInt();
        System.out.println("Transport with fits your request: ");
        Iterator<Vehicle> iterator = set.iterator();
        while (iterator.hasNext()) {
            Vehicle vehicle = iterator.next();
            if (vehicle.getPassengerSeats()>=number) {
                System.out.println(vehicle);
            }
        }
    }

    public void printSetVehicles(Set<Vehicle> set) {
        Iterator<Vehicle> iterator = set.iterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " || ");
        }
        System.out.println();
    }

 //Checked Exceptions
    public void readFromFile(File file) throws IOException {
        FileReader fr = null;
        BufferedReader br = null;
        Set<String> result;
        try {
            fr = new FileReader(file);
            br = new BufferedReader(fr);
            String line;
            result = new HashSet<>();
            while ((line = br.readLine()) != null) {
                result.add(line);
            }
        } catch (FileNotFoundException e) {
            System.err.println(file.getAbsolutePath() +
                    new CustomExceptions ("There are no vehicles :("));
        } catch (IOException e) {
            System.err.println(new CustomExceptions ("Something went wrong. Error ", e));
        } finally {
            if (fr != null) {
                fr.close();
            }
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    System.err.println(e);
                }
            }
        }
    }
}
