package homework_2.task_1;

import java.io.IOException;

public class CustomExceptions extends IOException {

        public CustomExceptions (String message) {
            super(message);
        }

        public CustomExceptions (String message, Throwable cause) {
            super(message, cause);
        }

        public CustomExceptions (Throwable cause) {
            super(cause);
        }
}
