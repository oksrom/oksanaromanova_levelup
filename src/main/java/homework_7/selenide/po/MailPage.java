package homework_7.selenide.po;

import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class MailPage {

    @Step("Написать новое письмо")
    public static void writeNewEmail(String email, String subject, String text) {
        $x("//*[text() ='Написать письмо']").click();
        $("[data-type='to'] input").sendKeys(email);
        $("[name='Subject']").sendKeys(subject);
        $("[role='textbox']").sendKeys(text);
    }

    @Step("Нажать на кнопку 'Сохранить'")
    public static void clickSaveButton() {
        $x("//*[text() ='Сохранить']").click();
    }

    @Step("Нажать на кнопку 'Закрыть'")
    public static void clickCloseButton() {
        $("[title ='Закрыть']").click();
    }

    @Step("Нажать на кнопку 'Отправить'")
    public static void clickSendButton() {
        $x("//*[text() ='Отправить']").click();
    }

    @Step("Нажать на кнопку 'Удалить'")
    public static void clickDeleteButton() {
        $("[title ='Удалить']").click();
    }

    @Step("Зайти в папку 'Черновики'")
    public static void enterDraftsFolder() {
       $x("//*[@href='/drafts/']").click();
    }

    @Step("Зайти в папку 'Отправленные'")
    public static void enterSentFolder() {
       $x( "//*[@href='/sent/']").click();
    }

    @Step("Зайти в папку 'Отправленные себе'")
    public static void enterTomyselfFolder() {
        $x("//*[@href='/tomyself/']").click();
    }

    @Step("Зайти в подпапку 'Тест'")
    public static void enterTestSubFolder() {
        $x("//div[@class='nav__folder-name']/div[text()='Тест']").click();
    }

    @Step("Зайти в корзину")
    public static void enterTrashFolder() {
        $x("//*[@href='/trash/']").click();
    }

    @Step("Выйти из почты")
    public static void logout() {
        $x("//a[@title='выход']");
    }

    @Step("Конпка 'Написать имейл' должна отображаться")
    public static void isWriteEmailDisplayed() {
         $x("//*[text() ='Написать письмо']").shouldBe(visible);;
    }

    @Step("Последний имейл в списке должен отображаться")
    public static void isLastEmailDisplayed() {
        $x("*//div[@class='llc__content']").shouldBe(visible);;
    }

    @Step("Кликнуть по последнему имейлу в списке")
    public static void clickOnLastEmail() {
        $x("*//div[@class='llc__content']").click();
    }

    @Step("Имейл адрес должен быть согласно вводу: '{0}'")
    public static void isEmailTheRightOne(String emailfill) {
        $x("//div[@class = 'llc__item llc__item_correspondent']").shouldHave(text(emailfill));
    }

    @Step("Непрочтенный имейл адрес должен быть согласно вводу: '{0}'")
    public static void isUnreadEmailTheRightOne(String emailfill) {
        $x("//div[@class ='llc__item llc__item_correspondent llc__item_unread']").shouldHave(text(emailfill));
    }

    @Step("Имейл адрес отправителя должен быть согласно вводу: '{0}'")
    public static void doesSenderUnreadEmailContains(String emailfill) {
        $x("//div[@class ='llc__item llc__item_correspondent llc__item_unread']").shouldHave(matchesText(emailfill.substring(1,5)));
    }

    @Step("Тема должна быть согласно вводу: '{0}'")
    public static void isSubjectTheRightOne(String subjectfill) {
         $x("//span[@class = 'llc__subject']").shouldHave(text(subjectfill));
    }

    @Step("Проверка, что имейл ушел из папки")
    public static void isItAnotherEmailBySubject (String subjectfill) {
        $x("//span[@class = 'llc__subject']").shouldNotHave(text(subjectfill));
    }

    @Step("Тема письма должна содержать конкретный текст: '{0}'")
    public static void doesSubjectTextContain (String subjectfill) {
        $x("//span[@class = 'llc__subject']").shouldHave(matchesText(subjectfill));
    }

    @Step("Тема непрочитанного имейла должна быть согласно вводу: '{0}'")
    public static void isUnreadSubjectTheRightOne(String subjectfill) {
         $x("//span[@class = 'llc__subject llc__subject_unread']").shouldHave(text(subjectfill));
    }

    @Step("Текст письма должен быть согласно вводу: '{0}'")
    public static void isTextTheRightOne(String textfill) {
       $x("//span[@class = 'll-sp__normal']").shouldHave(text(textfill));
    }

    @Step("Почта должа открыться")
    public static void mailPageOpened() {
       $x("//title").waitWhile(visible.matchesText("\\*Входящие\\*"), 15000);
    }
}
