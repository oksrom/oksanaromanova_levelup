package homework_7.selenide.po;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ReadFile {
    public static Properties readFromFile(String filePath) {
        Properties properties = new Properties();
        try {
            FileInputStream fis = new FileInputStream(filePath);
            properties.load(fis);
        } catch (
                IOException e) {
            System.err.println("Error: Property file is missing!");
        }
        return properties;
    }
}
