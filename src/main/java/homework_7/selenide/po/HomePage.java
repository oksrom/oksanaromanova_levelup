package homework_7.selenide.po;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

public class HomePage {
    private static final String URL = "https://mail.ru";

    @Step("Открывается домашняя страница 'https://mail.ru'")
    public static void open() {
        Selenide.open(URL);
    }

    @Step("Нажать на кнопку войти")
    public static void clickSubmitButton() {
        $(By.id("mailbox:submit")).click();
    }

    @Step("Ввести имейл и пароль")
    public static void login(String username, String password) {
        $(By.id("mailbox:login")).sendKeys(username);
        $(By.id("mailbox:submit")).click();
        $(By.id("mailbox:password")).sendKeys(password);
        $(By.id("mailbox:submit")).click();
    }

}
