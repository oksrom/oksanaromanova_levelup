package homework_3.task_2;

import java.util.*;

public class Dividers {
    public static void main(String[] args) {
        Dividers devRun = new Dividers();
        devRun.divide(1, 100000);
    }

    List<Integer> divide(int begin, int end) {
        List<Integer> div = new ArrayList<>(end - begin + 1);
        for (Integer i = begin; i <= end; i++) {
            div.add(i);
        }
        Collections.shuffle(div);
        System.out.println("\nПеремешанный список элементов : \n" + div);
        checkIfElementsUnique(div);
        divisibilityСheck(div);
        return div;
    }

    public void checkIfElementsUnique (List<Integer> div) {
        int p = 0;
        for (int i = 0; i < div.size(); i++) {
            for (int j = i + 1; j < div.size()-1; j++) {
                if (div.get(i).equals(div.get(j))) {
                    p++;
                }
            }
        }
        if (p == 0) {
            System.out.println("В последовательности все элементы уникальны");
        }
        else {
            System.out.println("В последовательности есть повторяющиеся элементы");
        }
    }

  public void divisibilityСheck(List<Integer> div) {
        TreeSet<Integer> numberDiv2 = new TreeSet<>();
        TreeSet<Integer> numberDiv3 = new TreeSet<>();
        TreeSet<Integer> numberDiv5 = new TreeSet<>();
        TreeSet<Integer> numberDiv7 = new TreeSet<>();
        Map<Integer, TreeSet<Integer>> map = new HashMap<>();
        for (int i=0; i < div.size(); i++) {
            if (div.get(i) % 2 == 0) {
                numberDiv2.add(div.get(i));
                map.put(2, numberDiv2);
            } else if (div.get(i) % 3 == 0) {
                numberDiv3.add(div.get(i));
                map.put(3, numberDiv3);
            } else if (div.get(i) % 5 == 0) {
                numberDiv5.add(div.get(i));
                map.put(5, numberDiv5);
            } else if (div.get(i) % 7 == 0) {
                numberDiv7.add(div.get(i));
                map.put(7, numberDiv7);
            }
        }
       printResult(map);
    }

    public void printResult(Map<Integer, TreeSet<Integer>> div) {
        Set<Map.Entry<Integer, TreeSet<Integer>>> numbers = div.entrySet();
        for (Map.Entry<Integer, TreeSet<Integer>> number : numbers) {
            System.out.println(String.format("На '%s' без остатка делятся '%s' \n", number.getKey(), number.getValue()));
        }
    }
}


